# README #

Dieses Repository beinhaltet als Module einmal den Vortrag auf reveal.js Basis und die zum Vortrag gehörende DemoApplication.

## Vortrag ##

Der Vortrag selbst liegt im Verzeichnis `vortrag/reveal.js-3.2.0`

Die Inhalte sind dort in der Datei `index.html` zu finden.

Technische Basis ist das [reveal.js](https://github.com/hakimel/reveal.js) Framework.

Die Slides von Stand heute (2015-12-09) sind in der PDF Datei im `vortrag` Verzeichnis zu finden.

## DemoApplication ##

Die Demo Applikation ist im `demo` Modul zu finden. Siehe dortige README.MD

