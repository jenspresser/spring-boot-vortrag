package com.example

import com.example.model.Action
import com.example.stats.Statistics
import spock.lang.Specification

/**
 * Created by jpresser on 07.01.2016.
 */
class CounterKeySpecification extends Specification {
    def "counter key for given action"() {
        given: "a CREATE action"
        def action = Action.CREATED

        when: "creating a counter key"
        def key = Statistics.CounterKey.keyFor(action)

        then: "should contain the action in lowercase"
        key == "counter_created"
    }

    def "global counter key when no action given"() {
        given: "a null action"
        Action action = null

        when: "creating a counter key"
        def key = Statistics.CounterKey.keyFor(action)

        then: "should contain global counter"
        key == "counter_global"
    }

}
