package com.example

import com.example.model.Action
import com.example.stats.Statistics
import org.springframework.data.redis.core.StringRedisTemplate
import org.springframework.data.redis.core.ValueOperations
import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by jpresser on 07.01.2016.
 */
class StatisticsSpecification extends Specification {
    ValueOperations valOps = Mock();
    StringRedisTemplate redis = Mock() {
        opsForValue() >> valOps
    }
    Statistics stats = new Statistics(redis);

    @Unroll
    def "should get counter #counter for #action"(int counter, Action action) {
        given: "a counter value saved in the backing data service (redis)"
        actionCounter(action, counter)

        when: "fetching the counter"
        def counts = stats.getCounts(action)

        then: "it should match the expected count"
        counts == counter

        where: "for a combination of an counter and an action"
        [counter, action] << [0..2, [Action.CREATED, Action.UPDATED]].combinations()
    }

    def "should get 0 counter when redis has no result"() {
        given:
        nullCounter(Action.CREATED)

        when:
        def counts = stats.getCounts(Action.CREATED)

        then:
        counts == 0
    }

    def "should update counter for action"() {
        when:
        stats.incrementCount(Action.CREATED)

        then:
        1 * valOps.increment("counter_created", 1)
    }

    private void actionCounter(Action action, int count) {
        valOps.get("counter_" + action.toString().toLowerCase()) >> String.valueOf(count)
    }

    private void nullCounter(Action action) {
        valOps.get("counter_" + action.toString().toLowerCase()) >> null
    }

}
