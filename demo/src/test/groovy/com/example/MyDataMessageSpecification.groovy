package com.example

import com.example.model.Action
import com.example.model.MyDataMessage
import spock.lang.Specification

/**
 * Created by jpresser on 07.01.2016.
 */
class MyDataMessageSpecification extends Specification {
    def "message string representation with action::message"() {
        given:
        def msg = new MyDataMessage(Action.CREATED, "message body");

        when:
        def string = msg.toString()

        then:
        string == "CREATED::message body"
    }

    def "create message object from String representation"() {
        given:
        def string = "CREATED::message body"

        when:
        def msg = MyDataMessage.fromString(string)

        then:
        msg.action == Action.CREATED
        msg.body == "message body"
    }
}
