package com.example;

import com.example.data.MyDataReceiver;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by jpresser on 17.11.2015.
 */
@Configuration
public class DemoConfiguration {
    private final RabbitTemplate rabbitTemplate;

    private final DemoSettings demoSettings;

    @Autowired
    public DemoConfiguration(RabbitTemplate rabbitTemplate, DemoSettings demoSettings) {
        this.rabbitTemplate = rabbitTemplate;
        this.demoSettings = demoSettings;
    }

    @Bean
    public TopicExchange topicExchange() {
        return new TopicExchange(demoSettings.getExchange());
    }

    @Bean
    public Queue queue() {
        return new Queue(demoSettings.getQueue());
    }

    @Bean
    public Binding binding(TopicExchange exchange, Queue queue) {
        return BindingBuilder.bind(queue).to(exchange).with(demoSettings.getQueue());
    }

    @Bean
    MessageListenerAdapter listenerAdapter(MyDataReceiver receiver) {
        return new MessageListenerAdapter(receiver, "receiveMessage");
    }

    @Bean
    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory, MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(demoSettings.getQueue());
        container.setMessageListener(listenerAdapter);
        return container;
    }
}
