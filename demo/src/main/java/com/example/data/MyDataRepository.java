package com.example.data;

import com.example.model.MyData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by jpresser on 05.11.2015.
 */
@Repository
public interface MyDataRepository extends JpaRepository<MyData, Long> {
    MyData findByName(String name);
}
