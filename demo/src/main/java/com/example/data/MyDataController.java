package com.example.data;

import com.example.DemoSettings;
import com.example.model.Action;
import com.example.model.MyData;
import com.example.model.MyDataMessage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static java.lang.String.format;

/**
 * Created by jpresser on 17.11.2015.
 */
@Controller
@RequestMapping("/data")
public class MyDataController {
    private static final Log log = LogFactory.getLog(MyDataController.class);

    private final MyDataRepository myDataRepository;

    private final RabbitTemplate rabbitTemplate;

    private final DemoSettings demoSettings;

    @Autowired
    public MyDataController(MyDataRepository myDataRepository, RabbitTemplate rabbitTemplate, DemoSettings demoSettings) {
        this.myDataRepository = myDataRepository;
        this.rabbitTemplate = rabbitTemplate;
        this.demoSettings = demoSettings;
    }

    @RequestMapping("/")
    @ResponseBody
    public List<MyData> list() {
        log.info("/data/ requested");

        return myDataRepository.findAll();
    }

    @RequestMapping("/{id}")
    @ResponseBody
    public MyData show(@PathVariable("id") Long id) {
        log.info(format("/data/%s requested", String.valueOf(id)));

        MyData myData = myDataRepository.findOne(id);

        if (myData == null) {
            throw new MyDataNotFoundException(id);
        }

        return myData;
    }

    @RequestMapping("/byName/{name}")
    @ResponseBody
    public MyData showByName(@PathVariable("name") String name) {
        log.info(format("/data/byName/%s requested", String.valueOf(name)));

        MyData myData = myDataRepository.findByName(name);

        if (myData == null) {
            throw new MyDataNotFoundException(name);
        }

        return myData;
    }

    @RequestMapping("/create")
    @ResponseBody
    public MyData create(@RequestParam String name, @RequestParam(required = false) String description) {
        log.info("/data/create requested");

        final String useDescription = Optional.ofNullable(description).orElse(name);

        MyData myData = myDataRepository.save(MyData.create(name, useDescription));

        MyDataMessage msg = MyDataMessage.withValues(Action.CREATED, myData.getId(), name, useDescription);

        rabbitTemplate.convertAndSend(demoSettings.getQueue(), msg.toString());

        return myData;
    }

    @RequestMapping("/{id}/update")
    @ResponseBody
    public MyData update(@PathVariable("id") Long id, @RequestParam(required = false) String name, @RequestParam(required = false) String description) {
        log.info(format("/data/%s/update requested", String.valueOf(id)));

        MyData myData = myDataRepository.findOne(id);

        if (myData == null) {
            throw new MyDataNotFoundException(id);
        }

        if (name != null) {
            myData.setName(name);
        }
        if (description != null) {
            myData.setDescription(description);
        }

        myDataRepository.save(myData);

        MyDataMessage msg = MyDataMessage.withValues(Action.UPDATED, id, name, description);

        rabbitTemplate.convertAndSend(demoSettings.getQueue(), msg.toString());

        return myData;
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "MyData not found")
    public static class MyDataNotFoundException extends RuntimeException {
        public MyDataNotFoundException(Long id) {
            super(format("MyData (id=%s) not found", id));
        }

        public MyDataNotFoundException(String name) {
            super(format("MyData not found by name [%s]", name));
        }
    }
}