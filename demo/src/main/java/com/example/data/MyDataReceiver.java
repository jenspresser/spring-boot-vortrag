package com.example.data;

import com.example.stats.Statistics;
import com.example.model.MyDataMessage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MyDataReceiver {
    private static final Log log = LogFactory.getLog(MyDataReceiver.class);

    private final Statistics stats;

    @Autowired
    public MyDataReceiver(Statistics stats) {
        this.stats = stats;
    }

    public void receiveMessage(String message) {
        MyDataMessage msg = MyDataMessage.fromString(message);

        stats.incrementCount(msg.action);

        log.info(String.format("Action (%s) received with message: [%s]", msg.action, msg.body));
    }

}