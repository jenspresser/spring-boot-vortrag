package com.example.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by jpresser on 05.11.2015.
 */
@Entity
public class MyData {
    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private String description;

    public MyData() {}

    public MyData(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public static MyData create(String name, String description) {
        return new MyData(name, description);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
