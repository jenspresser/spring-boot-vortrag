package com.example.model;

/**
 * Created by jpresser on 09.01.2016.
 */
public enum Action {
    CREATED, UPDATED
}
