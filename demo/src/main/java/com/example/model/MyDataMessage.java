package com.example.model;

import static java.lang.String.format;

/**
 * Created by jpresser on 07.01.2016.
 */
public class MyDataMessage {
    public final Action action;
    public final String body;

    public MyDataMessage(Action action, String body) {
        this.action = action;
        this.body = body;
    }

    public static MyDataMessage fromString(String msg) {
        String[] parts = msg.split("::");

        String action = parts[0];
        String body = parts[1];

        return new MyDataMessage(Action.valueOf(action), body);
    }

    public static MyDataMessage withValues(Action action, Long id, String name, String description) {
        String body = format("(id=%s name=%s description=%s)", id, name, description);

        return new MyDataMessage(action, body);
    }

    @Override
    public String toString() {
        return format("%s::%s", action.toString(), body);
    }
}
