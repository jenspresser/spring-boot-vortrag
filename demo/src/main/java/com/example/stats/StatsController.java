package com.example.stats;

import com.example.data.MyDataRepository;
import com.example.model.Action;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by jpresser on 17.11.2015.
 */
@Controller
@RequestMapping("/stats")
public class StatsController {
    private static final Log log = LogFactory.getLog(StatsController.class);

    private final MyDataRepository repository;

    private final Statistics stats;

    @Autowired
    public StatsController(MyDataRepository repository, Statistics stats) {
        this.repository = repository;
        this.stats = stats;
    }

    @RequestMapping("/")
    @ResponseBody

    public String stats() {
        log.info("/stats/ requested");

        long dataSize = repository.count();
        long createCounter = stats.getCounts(Action.CREATED);
        long updateCounter = stats.getCounts(Action.UPDATED);

        return String.format("# of data (%s), # of create events (%s), # of update events (%s)", dataSize, createCounter, updateCounter);
    }

}
