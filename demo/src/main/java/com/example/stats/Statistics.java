package com.example.stats;

import com.example.model.Action;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

/**
 * Created by jpresser on 07.01.2016.
 */
@Service
public class Statistics {
    private final StringRedisTemplate redis;

    @Autowired
    public Statistics(StringRedisTemplate redis) {
        this.redis = redis;
    }


    public long getCounts(Action action) {
        String key = CounterKey.keyFor(action);

        String counter = redis.opsForValue().get(key);

        if(counter==null ||counter.length() == 0) {
            return 0;
        }

        return Long.parseLong(counter);
    }

    public void incrementCount(Action action) {
        String key = CounterKey.keyFor(action);

        redis.opsForValue().increment(key, 1);
    }

    /**
     * Created by jpresser on 07.01.2016.
     */
    public static class CounterKey {
        public static String keyFor(Action action) {
            if (action == null) {
                return "counter_global";
            }

            return "counter_" + action.toString().toLowerCase();
        }
    }
}
